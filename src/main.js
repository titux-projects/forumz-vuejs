import Vue from 'vue'
import router from './router'
import store from './vuex'

Vue.component('app', require('./components/App.vue'))
Vue.component('navigation', require('./components/Navigation.vue'))

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<app/>',
  store: store,
  router: router
})
